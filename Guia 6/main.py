from garden import Garden
from alumno import Alumno
from time import sleep

# Se define un decorador para una funcion print
def print_decorator(func):
    def wrap(text):
        print("\n")
        print("="*72)
        func(text)
        print("="*72)
        print("\n")
    return wrap


# Esta funcion esta decorada, e imprime un string centrado en la pantalla
@print_decorator
def imprimir(text):
    espacios = (72 - len(text))//2
    text = " " * espacios + text
    print(text)


# Funcion principal del programa
def main():

    # La lista contiene los nombres de los 12 alumnos que seran creados luego
    nombres = [ "Alicia", "Marit", "Pepito", "David", "Eva", "Lucia",
                "Rocio", "Andres", "Jose", "Belen", "Sergio","Larry"]
    # Se ordena la lista alfabeticamente
    nombres.sort()

    # Se crea un objeto tipo garden
    garden = Garden()

    # Se crean los 12 alumnos y se asocian a garden
    for name in nombres:
        garden.alumnos = Alumno(name)

    # Se les reparten las plantas a cada niño
    garden.repartir_plantas()

    imprimir("Bienvenido al jardin")
    print("Uno por uno los niños reciben sus plantas:\n")

    # Se imprime por pantalla las plantas que le tocaron a cada alumno
    for kid in garden.alumnos:
        sleep(0.75)
        plantitas = []
        for item in garden.plantas(kid):
            plantitas.append(item.nombre)
        print(f" - {kid.nombre}: ", " - ".join(plantitas), "\n")

    # Este ciclo implementa un menu en el programa
    while True:
        imprimir("Opciones:")
        print(" < 1 > Ver todas las plantas en la ventana")
        print(" < 2 > Ver todas las plantas de un niño en especifico")
        print(" < 3 > Terminar ejecucion\n\n")

        # Se manejan excepciones
        try:
            opcion = int(input("Elija una opcion: "))
            if opcion not in (1, 2, 3):
                print("La opcion elegida no es valida, intente de nuevo.\n")
                continue
        except ValueError:
            print("La opcion elegida no es valida, intente de nuevo.\n")
            continue

        # Esta opcion muestra todas las plantas en la ventana
        if opcion == 1:
            print("\n")
            garden.ventana()

        # Esta opcion muestra todas las plantas en la ventana del niño elegido
        elif opcion == 2:
            while True:
                print("\nLa lista de alumnos es:\n")
                # Este ciclo imprime una lista de los alumnos en el garden
                for x in range(0, len(garden.alumnos)):
                    print(f"< {x+1} > {garden.alumnos[x].nombre}")
                # Se manejan excepciones
                try:
                    opcion = int(input("Elija una opcion: "))
                    if opcion < 0 or opcion > 12:
                        print("La opcion elegida no es valida, intente de nuevo.\n")
                    else:
                        print("\n")
                        garden.ventana(garden.alumnos[opcion-1])
                        break
                except ValueError:
                    print("La opcion elegida no es valida, intente de nuevo.\n")
                    
        # Esta opcion termina el ciclo, dandole fin al programa
        elif opcion == 3:
            break


if __name__ == "__main__":
    main()
