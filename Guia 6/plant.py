import alumno as a

# Se define la clase Plant
class Plant():
    def __init__(self):
        self._owner = None
        self._nombre = None

    # Se definen metodos set y get para el atributo self._owner
    @property
    def owner(self):
        return self._owner
    @owner.setter
    def owner(self, alumno):
        if isinstance(alumno, a.Alumno):
            self._owner = alumno
        else:
            raise ValueError(f"{alumno} no es un objeto de tipo Alumno")

    # Se un metodo get para el atributo self._nombre
    @property
    def nombre(self):
        return self._nombre
