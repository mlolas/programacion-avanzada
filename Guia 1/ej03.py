# Por: Magdalena Lolas y Diego Nuñez

# Se define una clase tipo Proteina
class Proteina():
	def __init__(self):
		self.clasificacion = "Proteina de Transporte"
		self.organismo = "Escherichia coli"
		self.sist_expr = "Escherichia coli"
		self.mutacion = "No"
		self.nombre = "Crystal Structure of Escherichia coli apo Pyridoxal 5'-phosphate homeostasis protein"
	
	# Metodo que muestra la info del objeto
	def mostrar_info(self):
		print(f"Nombre : {self.nombre}")
		print(f"Clasificacion : {self.clasificacion}")
		print(f"Organismo : {self.organismo}")
		print(f"Sistema de Expresion : {self.sist_expr}")
		print(f"Mutacion : {self.mutacion}")
		

# Funcion principal del programa
def main():
	7u9h = Proteina()


if __name__ = "__main__":
	main()
