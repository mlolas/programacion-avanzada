class Estudiante():

    def __init__(self, nombre):
        self._nombre = nombre
        self._asignatura = []
        self._diploma = []
        self._diplomas_propios = []


    @property
    def nombre(self):
        return self._nombre
    @nombre.setter
    def nombre(self, nombre):
        if isinstance(nombre, str):
            self._nombre = nombre
        else:
            print("El tipo de dato no corresponde")


    @property
    def asignatura(self):
        return self._asignatura
    @asignatura.setter
    def asignatura(self, asignatura):
        if isinstance(asignatura, Asignatura):
            self._asignatura.append(asignatura)
        else:
            print("El tipo de dato no corresponde")


    @property
    def diploma(self):
        return self._diploma
    @diploma.setter
    def diploma(self, diploma):
        if isinstance(diploma, Diploma):
            self._diploma.append(diploma)
        else:
            print("El tipo de dato no corresponde")


    @property
    def diploma_propio(self):
        return self._diplomas_propios
    @diploma_propio.setter
    def diploma_propio(self, diploma):
        if isinstance(diploma, Diploma):
            self._diplomas_propios.append(diploma)
        else:
            print("El tipo de dato no corresponde")


    def intercambio(self, other, index):
        x = self._diploma[0]
        self._diploma.remove(x)
        y = other._diploma[index]
        other._diploma.remove(y)
        self._diploma.append(y)
        other._diploma.append(x)


    def revision(self):
        diploma_eliminar = []
        for diploma in self._diploma:
            if diploma.estudiante.nombre == self._nombre:
                self._diplomas_propios.append(diploma)
                diploma_eliminar.append(diploma)
        for item in diploma_eliminar:
                self._diploma.remove(item)


class Asignatura():

    def __init__(self, nombre):
        self._nombre = nombre
        self._alumnos_inscritos = []


    @property
    def nombre(self):
        return self._nombre


    @property
    def alumnos(self):
        return self._alumnos_inscritos
    @alumnos.setter
    def alumnos(self, alumno):
        if isinstance(alumno, Estudiante):
            self._alumnos_inscritos.append(alumno)
        else:
            print("El tipo de dato no corresponde")


class Diploma():

    def __init__(self, estudiante, asignatura):
        if isinstance(estudiante, Estudiante):
            self._estudiante = estudiante
        else:
            print("no pertenece a clase adecuada")

        if isinstance(asignatura, Asignatura):
            self._asignatura = asignatura
        else:
            print("no pertenece a clase adecuada")


    @property
    def estudiante(self):
        return self._estudiante


    @property
    def asignatura(self):
        return self._asignatura
