# By Diego Núnez y Raimundo Oliva

import random as r
import time
from clases import Estudiante, Asignatura, Diploma

# Se define un decorador para una funcion print
def print_decorator(func):
    def wrap(text):
        print("========="*8)
        func(text)
        print("========="*8)
    return wrap

# Esta funcion esta decorada, e imprime un string
@print_decorator
def imprimir(text):
    print(text)

def main():

    # Se define una lista con nombres para los distintos alumnos que se crearan
    nombres = ["Sebastian", "Felipe", "Sofia", "Thomas", "Bastian", "Manuel",
               "Diego", "Alejandro", "Valentina", "Esteban", "Ana", "Matias",
               "Joaquin", "Raul", "Elena", "Valeria", "Maria", "Martina",
               "Cecilia", "Renato", "Fabio"]

    # Se define una lista con los nombres para crear las distintas asignaturas
    asignaturas = ["Fisica", "Programacion I", "Calculo II", "Termodinámica",
                   "Programacion Avanzada", "Algebra", "Quimica"]

    cant_alumnos = r.randint(8, len(nombres))

    lista_estudiantes = []
    lista_asignatura = []

    # Se crean los estudiantes
    for x in range(0, cant_alumnos):
        lista_estudiantes.append(Estudiante(nombres[x]))

    # Se crean las asignaturas
    for asignatura in asignaturas:
        lista_asignatura.append(Asignatura(asignatura))

    """
    - Un ramo no puede tener mas de 3 alumnos
        7 ramos * 3 alumnos x ramo = 21 diplomas
        21//cant_alumnos = int (cantidad de vueltas)
        21 % cant_alumnos = resto de diplomas
    """

    # Para que ningun alumno se quede sin asignaturas, en primera instancia
    # se reparten una cantidad x de asignaturas al azar a todos los estudiantes
    # y las vacantes que sobran se reparten al azar a algunos estudiantes
    vueltas_completas = (len(lista_asignatura)*3)//len(lista_estudiantes)
    resto = (len(lista_asignatura)*3) % len(lista_estudiantes)

    # Este ciclo asigna asignaturas a todos los alumnos
    for x in range(0, vueltas_completas):
        for alumno in lista_estudiantes:
            while True:
                # Se elije una asignatura al azar
                ramo = r.choice(lista_asignatura)
                # Si el alumno ya inscribio la asignatura, continua el ciclo
                if ramo in alumno.asignatura:
                    continue
                # Si la asignatura ya tiene 3 alumnos, continua el ciclo
                elif len(ramo.alumnos) == 3:
                    continue
                # Si no ocurren las anteriores, se inscribe la asignatura
                else:
                    alumno.asignatura = ramo
                    ramo.alumnos = alumno
                    break

    # Este ciclo termina de asignar las vacantes libres
    for x in range(0, resto):
        while True:
            alumno = r.choice(lista_estudiantes)
            ramo = r.choice(lista_asignatura)
            # Si el alumno ya inscribio la asignatura, continua el ciclo
            if ramo in alumno.asignatura:
                continue
            # Si la asignatura ya tiene 3 alumnos, continua el ciclo
            elif len(ramo.alumnos) == 3:
                continue
            # Si no ocurren las anteriores, se inscribe la asignatura
            else:
                alumno.asignatura = ramo
                ramo.alumnos = alumno
                break

    # Se crea una lista que contendra los diplomas, para luego repartirlos
    diplomas = []

    # Este ciclo crea los diplomas y los almacena en la lista
    for i in lista_estudiantes:
        for j in i.asignatura:
            diplomas.append(Diploma(i,j))

    # Este ciclo reparte de forma aleatorea los diplomas
    for alumno in lista_estudiantes:
        for x in range(0, len(alumno.asignatura)):
            d_entregado = r.choice(diplomas)
            diplomas.remove(d_entregado)
            alumno.diploma = d_entregado

    imprimir("\n\n La ceremonia empieza y se reparten los diplomas:\n\n")

    # Este ciclo imprime los diplomas que cada alumno tiene en posesion
    for alumno in lista_estudiantes:
        time.sleep(1.5)
        print(f" {alumno.nombre} recibe los siguientes diplomas:")
        for dip in alumno.diploma:
            print(f"\t{dip.estudiante.nombre} - {dip.asignatura.nombre}")
        print("--------"*6)

    imprimir("\n\n Terminada la ceremonia, los alumnos cambian sus diplomas:\n\n")

    # Este ciclo se encarga de cambiar los diplomas entre los estudiantes
    for num in range(0 ,len(lista_estudiantes)):
        alumno = lista_estudiantes[num]
        # Antes de cambiar sus diplomas, los alumnos revisan si es que
        # tienen uno propio para asi quedarselo y no cambiarlo
        alumno.revision()

        # Si el alumno no tiene diplomas para cambiar, pasa al siguiente
        if len(alumno.diploma) == 0:
            continue

        # Mientras el alumno tenga diplomas que cambiar, compara uno por uno con
        # el resto de sus compañeros, hasta que encuentra uno suyo y lo cambia
        while len(alumno.diploma) > 0:
            for x in range(num+1, len(lista_estudiantes)):
                alumno2 = lista_estudiantes[x]

                # Se revisan todos los diplomas del segundo alumno
                for dip in alumno2.diploma:
                    # Si uno de los diplomas es el del primer alumno, lo cambia
                    if dip.estudiante == alumno:
                        alumno.intercambio(alumno2, alumno2.diploma.index(dip))
                        time.sleep(1.5)
                        print(f"{alumno.nombre} cambia un diploma con {alumno2.nombre}")

                        # Ambos revisan sus diplomas buscando los propios
                        alumno.revision()
                        alumno2.revision()

                        # Si al primer alumno no le quedan diplomas por
                        # cambiar rompe el ciclo para pasar al siguiente
                        if len(alumno.diploma) == 0:
                            break
                if len(alumno.diploma) == 0:
                    break
            if len(alumno.diploma) == 0:
                break

    imprimir("\n\n Luego del intercambio, todos revisan los diplomas que tienen:\n\n")

    # Este ciclo imprime los diplomas de cada alumno despues de los intercambios
    for alumno in lista_estudiantes:
        time.sleep(1.5)
        print(f" {alumno.nombre} tiene los siguientes diplomas:")
        for dip in alumno.diploma_propio:
            print(f"\t{dip.estudiante.nombre} - {dip.asignatura.nombre}")
        print("--------"*6)


if __name__ == '__main__':
    main()
