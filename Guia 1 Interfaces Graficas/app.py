import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

from ventana_main import VentanaPrincipal

class Aplicacion():
    def __init__(self):
        self.ventana_principal = VentanaPrincipal()
        self.ventana_principal.set_default_size(800,600)
        self.ventana_principal.show_all()
        self.ventana_principal.connect("destroy", Gtk.main_quit)
