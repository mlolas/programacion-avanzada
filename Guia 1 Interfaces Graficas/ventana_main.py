import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DialogExample(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Resultado", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)

        self.label = Gtk.Label()

        box = self.get_content_area()
        box.add(self.label)

# Clase que reoresenta la ventana principal
class VentanaPrincipal(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_title("Sumatorias")

        self.box = Gtk.Box()
        self.col1 = Gtk.Box()
        self.col2 = Gtk.Box()

        self.col1.set_orientation(Gtk.Orientation.VERTICAL)
        self.col2.set_orientation(Gtk.Orientation.VERTICAL)

        self.label1 = Gtk.Label()
        self.entry1 = Gtk.Entry()
        self.boton1 = Gtk.Button()

        self.label1.set_label("Sumando 1")
        self.boton1.set_label("Reset")
        self.boton1.connect("clicked", self.boton_reset_clicked)

        self.col1.pack_start(self.label1, True, True, 0)
        self.col1.pack_start(self.entry1, True, True, 0)
        self.col1.pack_start(self.boton1, True, True, 0)

        self.label2 = Gtk.Label()
        self.entry2 = Gtk.Entry()
        self.boton2 = Gtk.Button()

        self.label2.set_label("Sumando 2")
        self.boton2.set_label("Aceptar")
        self.boton2.connect("clicked", self.boton_aceptar_clicked)

        self.col2.pack_start(self.label2, True, True, 0)
        self.col2.pack_start(self.entry2, True, True, 0)
        self.col2.pack_start(self.boton2, True, True, 0)

        self.box.pack_start(self.col1, True, True, 0)
        self.box.pack_start(self.col2, True, True, 0)
        self.add(self.box)

    def boton_reset_clicked(self, widget):
        self.entry1.set_text("")
        self.entry2.set_text("")

    def boton_aceptar_clicked(self, widget):
        dialog = DialogExample(self)
        sumando1 = self.entry1.get_text()
        sumando2 = self.entry2.get_text()
        if sumando1.isdigit():
            sumando1 = int(sumando1)
        else:
            sumando1 = len(sumando1)
        if sumando2.isdigit():
            sumando2 = int(sumando2)
        else:
            sumando2 = len(sumando2)
        dialog.label.set_label(f"{sumando1 + sumando2}")
        dialog.show_all()

        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            self.entry1.set_text("")
            self.entry2.set_text("")
            dialog.destroy()
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
        elif response == Gtk.ResponseType.DELETE_EVENT:
            dialog.destroy()
