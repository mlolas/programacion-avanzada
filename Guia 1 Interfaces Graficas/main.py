import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from app import Aplicacion


def main():
    app = Aplicacion()
    Gtk.main()


if __name__ == "__main__":
    main()
